package com.epam.esm.restadvancedpartthree.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.epam.esm.restadvancedpartthree"})
public class TestMysqlConfig {

    public static Properties getProperties() throws IOException {
        Properties properties = new Properties();
        FileReader file = new FileReader(
                TestMysqlConfig.class.getResource("/application-test.properties").getFile());
        properties.load(file);
        return properties;
    }

    @Profile("test-mysql")
    @Bean
    public DataSource dataSource() throws IOException {
        BasicDataSource basicDataSource = new BasicDataSource();
        Properties properties = getProperties();
        basicDataSource.setDriverClassName(properties.getProperty("spring.datasource.driverClassName"));
        basicDataSource.setUrl(properties.getProperty("spring.datasource.url"));
        basicDataSource.setMaxIdle(Integer.parseInt(properties.getProperty("spring.datasource.maxIdle")));
        basicDataSource.setMaxWaitMillis(Long.parseLong(properties.getProperty("spring.datasource.maxWait")));
        basicDataSource.setMaxTotal(Integer.parseInt(properties.getProperty("spring.datasource.maxActive")));
        basicDataSource.setUsername(properties.getProperty("spring.datasource.user"));
        basicDataSource.setPassword(properties.getProperty("spring.datasource.password"));
        return basicDataSource;
    }

    @Profile("test-mysql")
    @Bean(name = "transactionManager")
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Profile("test-mysql")
    @Bean
    public Flyway getFlyway() throws IOException {
        org.flywaydb.core.api.configuration.Configuration configuration = new FluentConfiguration()
                .dataSource(dataSource())
                .configuration(getProperties());
        return new Flyway(configuration);
    }
}
