package com.epam.esm.restadvancedpartthree.repository.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.GiftCertificateRepository;
import com.epam.esm.restadvancedpartthree.repository.OrderRepository;
import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import com.epam.esm.restadvancedpartthree.repository.entity.OrderStatus;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class OrderRepositoryImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void setup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveOrder() {
        long giftCertificateId = 2L;
        Optional<GiftCertificate> giftCertificate = giftCertificateRepository.findById(giftCertificateId);
        assertTrue(giftCertificate.isPresent());

        long userId = 1L;
        Optional<User> user = userRepository.findById(userId);
        assertTrue(user.isPresent());

        int quantity = 10;
        Order order = new Order(quantity, OrderStatus.UNPAID, user.get(), giftCertificate.get());
        order.setTotalCost(order.getUnitPrice() * order.getQuantity());
        Order saveOrder = orderRepository.save(order);

        assertOrder(order, saveOrder);
    }
    @Test
    public void shouldFindOrderById() {
        long orderId = 3L;
        Optional<Order> order = orderRepository.findById(orderId);
        assertTrue(order.isPresent());
        assertEquals(orderId, order.get().getId());
    }

    @Test
    public void shouldReturnEmptyWhenFindOrderById() {
        long orderId = 7L;
        Optional<Order> orderOptional = orderRepository.findById(orderId);
        assertFalse(orderOptional.isPresent());
    }

    @Test
    public void findOrdersByGiftCertificateId() {
        long giftCertificateId = 1L;

        Page<Order> orderPage = orderRepository.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(0, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindGiftCertificateId() {
        long giftCertificateId = 6L;
        Page<Order> giftCertificatePage = orderRepository.findByGiftCertificateId(giftCertificateId,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindOrdersByOwnerId() {
        long userId = 2L;
        Page<Order> orderPage = orderRepository.findByOwnerId(
                userId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(0, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyWhenFindOwnerId() {
        long ownerId = 5L;
        Page<Order> orderPage = orderRepository.findByOwnerId(ownerId, PageRequest.of(
                DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, orderPage.getTotalElements());
    }

    private void assertOrder(Order expectedOrder, Order actualOrder) {
        assertEquals(expectedOrder.getId(), actualOrder.getId());
        assertEquals(expectedOrder.getQuantity(), actualOrder.getQuantity());
        assertEquals(expectedOrder.getStatus(), actualOrder.getStatus());
        assertEquals(expectedOrder.getTotalCost(), actualOrder.getTotalCost());
        assertEquals(expectedOrder.getOwner().getId(), actualOrder.getOwner().getId());
        assertEquals(expectedOrder.getGiftCertificate().getId(), actualOrder.getGiftCertificate().getId());
        assertEquals(expectedOrder.getOrderedDate(), actualOrder.getOrderedDate());
    }

}