package com.epam.esm.restadvancedpartthree.repository.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class UserRepositoryImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    UserRepository userRepository;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void setup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveUser() {
        User user = new User();
        user.setFullName("Beckham Tony");
        user.setEmail("beckham_tony@email.com");
        user.setPassword("beckham_tony");
        User saveUser = userRepository.save(user);
        assertUser(user, saveUser);
    }

    @Test
    public void shouldFindAll() {
        Page<User> userPage = userRepository.findAll(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(5, userPage.getTotalElements());
        assertEquals(2, userPage.getTotalPages());
        assertEquals(3, userPage.getNumberOfElements());
        assertEquals(0, userPage.getNumber());

        assertTrue(userPage.hasContent());
        assertTrue(userPage.hasNext());
        assertFalse(userPage.hasPrevious());

        Page<User> userPage2 = userRepository.findAll(userPage.nextPageable());
        assertEquals(5, userPage2.getTotalElements());
        assertEquals(2, userPage2.getTotalPages());
        assertEquals(2, userPage2.getNumberOfElements());
        assertEquals(1, userPage2.getNumber());

        assertTrue(userPage2.hasContent());
        assertFalse(userPage2.hasNext());
        assertTrue(userPage2.hasPrevious());

        Page<User> userPage3 = userRepository.findAll(userPage2.previousPageable());
        assertEquals(5, userPage3.getTotalElements());
        assertEquals(2, userPage3.getTotalPages());
        assertEquals(3, userPage3.getNumberOfElements());
        assertEquals(0, userPage3.getNumber());

        assertTrue(userPage3.hasContent());
        assertTrue(userPage3.hasNext());
        assertFalse(userPage3.hasPrevious());
    }

    @Test
    public void shouldFindUserById() {
        long userId = 2L;
        Optional<User> userOptional = userRepository.findById(userId);
        assertTrue(userOptional.isPresent());
        assertEquals(userId, userOptional.get().getId());
    }

    @Test
    public void shouldReturnEmptyWhenFindUserById() {
        long userId = 6L;
        Optional<User> userOptional = userRepository.findById(userId);
        assertFalse(userOptional.isPresent());
    }

    @Test
    public void shouldDeleteUserById() {
        long userId = 1L;
        userRepository.deleteById(userId);
        assertThrows(EmptyResultDataAccessException.class, () -> {
            userRepository.deleteById(userId);
        });
    }

    @Test
    public void shouldThrowExceptionWhenDeleteUser() {
        long userId = 6L;
        assertThrows(EmptyResultDataAccessException.class, ()->{
            userRepository.deleteById(userId);
        });
    }

    private void assertUser(User expectedUser, User actualUser) {
        assertEquals(expectedUser.getId(), actualUser.getId());
        assertEquals(expectedUser.getFullName(), actualUser.getFullName());
        assertEquals(expectedUser.getEmail(), actualUser.getEmail());
        assertEquals(expectedUser.getPassword(), actualUser.getPassword());
        assertEquals(expectedUser.getCreatedDate(), actualUser.getCreatedDate());
    }
}