package com.epam.esm.restadvancedpartthree.repository.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.GiftCertificateRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class GiftCertificateRepositoryTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void setup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldFindAll() {
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.
                findAll(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.
                findAll(giftCertificatePage1.nextPageable());

        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
    }

    @Test
    public void shouldSaveGiftCertificate() {
        GiftCertificate newGC = new GiftCertificate();
        newGC.setName("GC-four");
        newGC.setDescription("gift certificate four for integration test");
        newGC.setPrice(14.5);
        newGC.setDuration(40);
        Tag tag1 = new Tag();
        tag1.setName("apple");
        newGC.getTags().addAll(Collections.singleton(tag1));

        GiftCertificate saveGC1 = giftCertificateRepository.save(newGC);
        assertGiftCertificate(newGC, saveGC1);
    }

    @Test
    public void shouldFindAllGiftCertificates() {
        Page<GiftCertificate> gcPage1 = giftCertificateRepository.
                findAll(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(6, gcPage1.getTotalElements());
        assertEquals(2, gcPage1.getTotalPages());
        assertEquals(0, gcPage1.getNumber());
        assertEquals(3, gcPage1.getNumberOfElements());

        assertTrue(gcPage1.hasContent());
        assertTrue(gcPage1.hasNext());
        boolean condition = gcPage1.hasPrevious();
        assertFalse(condition);

        Page<GiftCertificate> gcPage2 = giftCertificateRepository.
                findAll(gcPage1.getPageable().next());
        assertEquals(6, gcPage2.getTotalElements());
        assertEquals(2, gcPage2.getTotalPages());
        assertEquals(1, gcPage2.getNumber());
        assertEquals(3, gcPage2.getNumberOfElements());

        assertTrue(gcPage2.hasContent());
        assertFalse(gcPage2.hasNext());
        assertTrue(gcPage2.hasPrevious());

        Page<GiftCertificate> gcPage4 = giftCertificateRepository.
                findAll(gcPage2.getPageable().previousOrFirst());
        assertEquals(6, gcPage4.getTotalElements());
        assertEquals(2, gcPage4.getTotalPages());
        assertEquals(0, gcPage4.getNumber());
        assertEquals(3, gcPage4.getNumberOfElements());

        assertTrue(gcPage4.hasContent());
        assertTrue(gcPage4.hasNext());
        assertFalse(gcPage4.hasPrevious());
    }

    @Test
    public void shouldFindGiftCertificateById() {
        long id = 2L;
        Optional<GiftCertificate> giftCertificate = giftCertificateRepository.findById(id);
        assertNotNull(giftCertificate, "GiftCertificate should not be null");
        assertTrue(giftCertificate.isPresent());
        assertEquals(id, giftCertificate.get().getId());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificateById() {
        long giftCertificateId = 19L;
        Optional<GiftCertificate> giftCertificate = giftCertificateRepository.findById(giftCertificateId);
        assertFalse(giftCertificate.isPresent());
    }

    @Test
    public void shouldUpdateGiftCertificateById() {
        long id = 1L;
        Optional<GiftCertificate> giftCertificateOptional = giftCertificateRepository.findById(id);
        assertTrue(giftCertificateOptional.isPresent());
        GiftCertificate giftCertificate = giftCertificateOptional.get();
        giftCertificate.setName("updated gc1");
        giftCertificate.setDescription("updated gift certificate by id in integration test");

        GiftCertificate updatedGC = giftCertificateRepository.save(giftCertificate);
        assertNotNull(updatedGC, "Updated gift certificate should be not null");
        assertGiftCertificate(giftCertificate, updatedGC);
    }

    @Test
    public void shouldDeleteGiftCertificateById() {
        long giftCertificateId = 2L;
        giftCertificateRepository.deleteById(giftCertificateId);
        Optional<GiftCertificate> giftCertificateOptional = giftCertificateRepository.findById(giftCertificateId);
        assertFalse(giftCertificateOptional.isPresent());
    }

    @Test
    public void shouldThrowExceptionWhenDeleteGiftCertificateById() {
        long giftCertificateId = 10L;
        assertThrows(EmptyResultDataAccessException.class, () -> {
            giftCertificateRepository.deleteById(giftCertificateId);
        });
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByDate() {
        String[] tagNames = {"five"};

        Page<GiftCertificate> giftCertificates = giftCertificateRepository.
                findByTagNames(
                        tagNames, tagNames.length,
                        PageRequest.of(DEFAULT_PAGE_NUMBER,DEFAULT_PAGE_SIZE));

        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(3, giftCertificates.getNumberOfElements());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.
                findByTagNames(
                        tagNames, tagNames.length, giftCertificates.nextPageable());

        assertNotNull(giftCertificatePage2);
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.
                findByTagNames(
                        tagNames, tagNames.length, giftCertificatePage2.previousPageable());

        assertNotNull(giftCertificatePage3);
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(0, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByName() {
        String sort = "gift_certificate.name ASC";
        String[] tagNames = {"five"};
        Page<GiftCertificate> giftCertificates = giftCertificateRepository.findByTagNames(
                tagNames,tagNames.length, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by(Sort.Direction.ASC, "gift_certificate.name")));

        assertNotNull(giftCertificates);
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(3, giftCertificates.getNumberOfElements());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("Gift Certificate Four", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate One", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.
                findByTagNames(tagNames, tagNames.length, giftCertificates.nextPageable());

        assertNotNull(giftCertificatePage2);
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("Gift Certificate Two", giftCertificatePage2.getContent().get(0).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.
                findByTagNames(tagNames, tagNames.length, giftCertificatePage2.previousPageable());

        assertNotNull(giftCertificatePage3);
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(0, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());

        assertEquals("Gift Certificate Four", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("Gift Certificate One", giftCertificatePage3.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage3.getContent().get(2).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByDateAndName() {
        String[] tagNames = {"five"};

        int tagNamesCount = 1;
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findByTagNames(
                tagNames, tagNamesCount, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.created_date").and(Sort.by("gift_certificate.name").descending())));

        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("Gift Certificate Two", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate One", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findByTagNames(
                tagNames, tagNamesCount, giftCertificatePage1.nextPageable());

        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("Gift Certificate Four", giftCertificatePage2.getContent().get(0).getName());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindGiftCertificatesAndTagsByTagName() {
        String[] tagNames = {"programming"};
        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.findByTagNames(
                tagNames, tagNames.length, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.created_date").and(Sort.by("gift_certificate.name").descending())));

        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByTagName() {
        String[] tagNames = {"three", "five"};

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findByTagNames(
                tagNames, tagNames.length, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(2, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindGiftCertificatesByTagName() {
        String[] tagNames = {"data structure"};
        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.findByTagNames(
                tagNames, tagNames.length, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfNameAndSortByDate() {
        String partOfName = "Gift";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findByNameContaining(
                partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.created_date")));
        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("Gift Certificate One", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Two", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findByNameContaining(
                partOfName, giftCertificatePage1.nextPageable());
        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("Gift Certificate Four", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("Gift Certificate Five", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("Gift Certificate Six", giftCertificatePage2.getContent().get(2).getName());


        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findByNameContaining(
                partOfName, giftCertificatePage2.previousPageable());
        assertEquals(6, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(0, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());

        assertEquals("Gift Certificate One", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("Gift Certificate Two", giftCertificatePage3.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage3.getContent().get(2).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfNameAndSortByName() {
        String partOfName = "Gift";
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.
                findByNameContaining(partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.name").ascending()));
        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("Gift Certificate Five", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate One", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findByNameContaining(
                partOfName, giftCertificatePage1.nextPageable());
        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("Gift Certificate Six", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("Gift Certificate Two", giftCertificatePage2.getContent().get(2).getName());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfName() {
        String partOfName = "moon";

        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.findByNameContaining(
                partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.created_date").and(Sort.by("gift_certificate.name").descending())));
        assertEquals(0, giftCertificatePage.getTotalElements());

    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfName() {
        String partOfName = "Tw";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findByNameContaining(
                        partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(1, giftCertificatePage1.getTotalElements());
        assertEquals(1, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesByPartOfName() {
        String partOfName = "&?";
        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.findByNameContaining(partOfName,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfDescAndSortByDate() {
        String partOfDesc = "st";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.
                findByDescriptionContaining(partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.created_date")));
        assertEquals(1, giftCertificatePage1.getTotalElements());
        assertEquals(1, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfDescAndSortByName() {
        String partOfDesc = "Certificate";

        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.
                findByDescriptionContaining(partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.name").ascending()));

        assertEquals(6, giftCertificatePage.getTotalElements());
        assertEquals(3, giftCertificatePage.getNumberOfElements());
        assertEquals(0, giftCertificatePage.getNumber());
        assertEquals(2, giftCertificatePage.getTotalPages());

        assertEquals("Gift Certificate Five", giftCertificatePage.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage.getContent().get(1).getName());
        assertEquals("Gift Certificate One", giftCertificatePage.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.
                findByDescriptionContaining(partOfDesc, giftCertificatePage.nextPageable());

        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());

        assertEquals("Gift Certificate Six", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("Gift Certificate Two", giftCertificatePage2.getContent().get(2).getName());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfDesc() {
        String partOfDesc = "algorithm";

        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.findByDescriptionContaining(partOfDesc,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("gift_certificate.created_date").
                                and(Sort.by("gift_certificate.name").descending())));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfDesc() {
        String partOfDesc = "Sixth";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(1, giftCertificatePage1.getTotalElements());
        assertEquals(1, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificatesByPartOfDesc() {
        String partOfDesc = "trump and clinton";
        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesByTagId() {
        long tagId = 5L;
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findByTagId(
                        tagId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findByTagId(
                tagId, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findByTagId(
                tagId, giftCertificatePage2.previousPageable());
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(0, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificateByTagId() {
        long tagId = 10L;
        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.
                findByTagId(tagId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    private void assertGiftCertificate(GiftCertificate expectedGiftCertificate, GiftCertificate actualGiftCertificate) {
        assertEquals(expectedGiftCertificate.getId(), actualGiftCertificate.getId());
        assertEquals(expectedGiftCertificate.getName(), actualGiftCertificate.getName());
        assertEquals(expectedGiftCertificate.getDescription(), actualGiftCertificate.getDescription());
        assertEquals(expectedGiftCertificate.getPrice(), actualGiftCertificate.getPrice());
        assertEquals(expectedGiftCertificate.getDuration(), actualGiftCertificate.getDuration());
        assertEquals(expectedGiftCertificate.getCreatedDate(), actualGiftCertificate.getCreatedDate());
    }

}