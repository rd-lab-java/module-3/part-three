package com.epam.esm.restadvancedpartthree.repository.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.GiftCertificateRepository;
import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class TagRepositoryTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    Flyway flyway;

    @AfterEach
    public void tearDown() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveTag() {
        String tagName = "seven";
        Tag tagFour = new Tag();
        tagFour.setName(tagName);

        Tag savedTagFour = tagRepository.save(tagFour);
        assertTag(tagFour, savedTagFour);
    }

    @Test
    public void shouldFindTagByName() {
        String tagName = "one";
        Optional<Tag> tagOptional = tagRepository.findByName(tagName);
        assertTrue(tagOptional.isPresent());
        assertEquals(tagName, tagOptional.get().getName());
    }

    @Test
    public void shouldReturnEmptyExceptionWhenFindTagByName() {
        String tagName = "geeks";
        Optional<Tag> tagOptional = tagRepository.findByName(tagName);
        assertFalse(tagOptional.isPresent());
    }

    @Test
    public void shouldFindTagsByGiftCertificateId() {

        long giftCertificateId = 4L;
        Page<Tag> tagPage1 = tagRepository.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(3, tagPage1.getTotalElements());
        assertEquals(1, tagPage1.getTotalPages());
        assertEquals(0, tagPage1.getNumber());
        assertEquals(3, tagPage1.getNumberOfElements());

        assertTrue(tagPage1.hasContent());
        assertFalse(tagPage1.hasNext());
        assertFalse(tagPage1.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindTagsByGiftCertificateId() {
        long giftCertificateId = 7L;
        Page<Tag> giftCertificatePage = tagRepository.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindAllTags() {
        Page<Tag> tagPage1 = tagRepository.findAll(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(10, tagPage1.getTotalElements());
        assertEquals(4, tagPage1.getTotalPages());
        assertEquals(0, tagPage1.getNumber());
        assertEquals(3, tagPage1.getNumberOfElements());

        assertTrue(tagPage1.hasContent());
        assertTrue(tagPage1.hasNext());
        assertFalse(tagPage1.hasPrevious());

        Page<Tag> tagPage2 = tagRepository.findAll(tagPage1.nextPageable());
        assertEquals(10, tagPage2.getTotalElements());
        assertEquals(4, tagPage2.getTotalPages());
        assertEquals(1, tagPage2.getNumber());
        assertEquals(3, tagPage2.getNumberOfElements());

        assertTrue(tagPage2.hasContent());
        assertTrue(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());

        Page<Tag> tagPage3 = tagRepository.findAll(tagPage2.previousPageable());
        assertEquals(10, tagPage3.getTotalElements());
        assertEquals(4, tagPage3.getTotalPages());
        assertEquals(0, tagPage3.getNumber());
        assertEquals(3, tagPage3.getNumberOfElements());

        assertTrue(tagPage3.hasContent());
        assertTrue(tagPage3.hasNext());
        assertFalse(tagPage3.hasPrevious());

        Page<Tag> tagPage4 = tagRepository.findAll(tagPage2.nextPageable());
        assertEquals(10, tagPage4.getTotalElements());
        assertEquals(4, tagPage4.getTotalPages());
        assertEquals(2, tagPage4.getNumber());
        assertEquals(3, tagPage4.getNumberOfElements());

        assertTrue(tagPage4.hasContent());
        assertTrue(tagPage4.hasNext());
        assertTrue(tagPage4.hasPrevious());

        Page<Tag> tagPage5 = tagRepository.findAll(tagPage4.nextPageable());
        assertEquals(10, tagPage5.getTotalElements());
        assertEquals(4, tagPage5.getTotalPages());
        assertEquals(3, tagPage5.getNumber());
        assertEquals(1, tagPage5.getNumberOfElements());

        assertTrue(tagPage5.hasContent());
        assertFalse(tagPage5.hasNext());
        assertTrue(tagPage5.hasPrevious());
    }

    @Test
    public void shouldDeleteTagById() {
        long tagId = 2L;
        tagRepository.deleteById(tagId);
        Optional<Tag> tagOptional = tagRepository.findById(tagId);
        assertFalse(tagOptional.isPresent());
    }

    @Test
    public void shouldThrowNotFoundException() {
        long tagId = 11L;
        assertThrows(EmptyResultDataAccessException.class, () -> {
            tagRepository.deleteById(tagId);
        });
    }

    @Test
    public void shouldFindTagById() {
        long tagId = 3L;
        Optional<Tag> tagOptional = tagRepository.findById(tagId);
        assertTrue(tagOptional.isPresent());
        assertEquals(tagId, tagOptional.get().getId());
    }

    @Test
    public void shouldEmptyTagWhenFindTagById() {
        long tagId = 11L;
        Optional<Tag> tagOptional = tagRepository.findById(tagId);
        assertFalse(tagOptional.isPresent());
    }

    @Test
    public void shouldFindMostUsedTags() {
        Page<Tag> tagPage = tagRepository.findMostUsedTags(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(8, tagPage.getTotalElements());
        assertEquals(3, tagPage.getNumberOfElements());
        assertEquals(3, tagPage.getTotalPages());
        assertEquals(0, tagPage.getNumber());

        assertTrue(tagPage.hasContent());
        assertTrue(tagPage.hasNext());
        assertFalse(tagPage.hasPrevious());

//        assertEquals("eight", tagPage.getContent().get(0).getName());

        Page<Tag> tagPage2 = tagRepository.findMostUsedTags(
                tagPage.nextPageable());
        assertEquals(8, tagPage2.getTotalElements());
        assertEquals(3, tagPage2.getNumberOfElements());
        assertEquals(3, tagPage2.getTotalPages());
        assertEquals(1, tagPage2.getNumber());

        assertTrue(tagPage2.hasContent());
        assertTrue(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());
    }

    @Test
    public void shouldFindTopMostUsedTags() {
        List<Tag> topTags = tagRepository.findTopNumberMostUsedTags(5);
        assertEquals(5, topTags.size());
    }

    private void assertTag(Tag expectedTag, Tag actualTag) {
        assertEquals(expectedTag.getId(), actualTag.getId());
        assertEquals(expectedTag.getName(), actualTag.getName());
    }
}