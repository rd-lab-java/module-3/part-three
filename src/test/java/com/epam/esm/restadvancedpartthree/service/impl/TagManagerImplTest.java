package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import com.epam.esm.restadvancedpartthree.service.TagManager;
import com.epam.esm.restadvancedpartthree.service.exception.TagNotFoundException;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class TagManagerImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    TagManager tagManager;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    Environment environment;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void init() {
        tagManager = new TagManagerImpl(tagRepository, environment);
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveTag() {
        Tag tag = new Tag();
        tag.setName("Programming");
        Tag saveTag = tagManager.save(tag);
        assertTag(tag, saveTag);
    }

    @Test
    public void shouldFindTagById() {
        long tagId = 2L;
        Tag tag = tagManager.findById(tagId);
        assertEquals(tagId, tag.getId());
    }

    @Test
    public void shouldReturnEmptyWhenFindTag() {
        long tagId = 11L;
        assertThrows(TagNotFoundException.class, () -> {
            tagManager.findById(tagId);
        });
    }

    @Test
    public void shouldDeleteTag() {
        long tagId = 2L;
        Tag deletedTag = tagManager.deleteById(tagId);
        assertEquals(tagId, deletedTag.getId());
        assertThrows(TagNotFoundException.class, ()-> {
            tagManager.deleteById(tagId);
        });
    }

    @Test
    public void shouldThrowExceptionWhenDeleteTag() {
        long tagId = 11L;
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
            tagManager.deleteById(tagId);
        });
    }

    @Test
    public void shouldFindAll() {
        Page<Tag> tagPage = tagManager.findAll(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(10, tagPage.getTotalElements());
        assertEquals(4, tagPage.getTotalPages());
        assertEquals(0, tagPage.getNumber());
        assertEquals(3, tagPage.getNumberOfElements());

        assertTrue(tagPage.hasContent());
        assertTrue(tagPage.hasNext());
        assertFalse(tagPage.hasPrevious());

        Page<Tag> tagPage2 = tagManager.findAll(tagPage.nextPageable());
        assertEquals(10, tagPage2.getTotalElements());
        assertEquals(4, tagPage2.getTotalPages());
        assertEquals(1, tagPage2.getNumber());
        assertEquals(3, tagPage2.getNumberOfElements());

        assertTrue(tagPage2.hasContent());
        assertTrue(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());
    }
    @Test
    public void shouldFindTagsByGiftCertificateId() {
        long giftCertificateId = 5L;
        Page<Tag> tagPage = tagManager.findByGiftCertificateId(giftCertificateId,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(2, tagPage.getTotalElements());
        assertEquals(1, tagPage.getTotalPages());
        assertEquals(2, tagPage.getNumberOfElements());
        assertEquals(0, tagPage.getNumber());
    }

    @Test
    public void shouldReturnEmptyWhenFindTags() {
        long giftCertificateId = 10L;
        Page<Tag> tagPage = tagManager.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, tagPage.getTotalElements());
    }

    @Test
    public void shouldFindMostUsedTags() {
        Page<Tag> tagPage = tagManager.findMostUsedTags(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(8, tagPage.getTotalElements());
        assertEquals(3, tagPage.getNumberOfElements());
        assertEquals(3, tagPage.getTotalPages());
        assertEquals(0, tagPage.getNumber());

        assertTrue(tagPage.hasContent());
        assertTrue(tagPage.hasNext());
        assertFalse(tagPage.hasPrevious());

        Page<Tag> tagPage2 = tagManager.findMostUsedTags(
                tagPage.nextPageable());
        assertEquals(8, tagPage2.getTotalElements());
        assertEquals(3, tagPage2.getNumberOfElements());
        assertEquals(3, tagPage2.getTotalPages());
        assertEquals(1, tagPage2.getNumber());

        assertTrue(tagPage2.hasContent());
        assertTrue(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());

        Page<Tag> tagPage3 = tagManager.findMostUsedTags(tagPage2.nextPageable());
        assertEquals(8, tagPage3.getTotalElements());
        assertEquals(2, tagPage3.getNumberOfElements());
        assertEquals(3, tagPage3.getTotalPages());
        assertEquals(2, tagPage3.getNumber());

        assertTrue(tagPage3.hasContent());
        assertFalse(tagPage3.hasNext());
        assertTrue(tagPage3.hasPrevious());
    }

    @Test
    public void shouldFindTopMostUsedTags() {
        List<Tag> topTags = tagManager.findTopMostUsedTags(5);
        assertEquals(5, topTags.size());
    }

    private void assertTag(Tag expectedTag, Tag actualTag) {
        assertEquals(expectedTag.getId(), actualTag.getId());
        assertEquals(expectedTag.getName(), actualTag.getName());
    }
}