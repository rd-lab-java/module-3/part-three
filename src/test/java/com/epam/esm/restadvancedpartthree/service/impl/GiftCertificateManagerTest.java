package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.GiftCertificateRepository;
import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import com.epam.esm.restadvancedpartthree.service.GiftCertificateManager;
import com.epam.esm.restadvancedpartthree.service.exception.GiftCertificateNotFoundException;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class GiftCertificateManagerTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    GiftCertificateManager giftCertificateManager;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    Environment environment;
    @Autowired
    EntityManager entityManager;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void init() {
        giftCertificateManager = new GiftCertificateManagerImpl(giftCertificateRepository, tagRepository, environment);
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldFindGiftCertificatesWithDateSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"five"});
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByTagNames(tagNames, PageRequest.of(
                DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("created_date")));
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertTrue(giftCertificatePage1.hasContent());
        assertEquals("Gift Certificate One", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Two", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findByTagNames(
                tagNames, giftCertificatePage1.nextPageable());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertTrue(giftCertificatePage2.hasContent());
        assertEquals("Gift Certificate Four", giftCertificatePage2.getContent().get(0).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findByTagNames(
                tagNames, giftCertificatePage2.previousPageable());
        assertEquals(0, giftCertificatePage3.getNumber());
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());

        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());
        assertTrue(giftCertificatePage3.hasContent());
    }

    @Test
    public void shouldFindGiftCertificatesWithNameASCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"five"});
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                        tagNames, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("name").ascending()));
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(3,giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        assertEquals("Gift Certificate Four", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate One", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByTagNames(
                tagNames, giftCertificates.nextPageable());
        assertEquals(4, giftCertificates2.getTotalElements());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1,giftCertificates2.getNumberOfElements());

        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());
        assertTrue(giftCertificates2.hasContent());

        assertEquals("Gift Certificate Two", giftCertificates2.getContent().get(0).getName());
    }

    @Test
    public void shouldFindGiftCertificatesWithNameDESCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"five"});
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                tagNames, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("name").descending()));
        assertNotNull(giftCertificates);
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(3,giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        assertEquals("Gift Certificate Two", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate One", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByTagNames(
                tagNames, giftCertificates.nextPageable());
        assertEquals(4, giftCertificates2.getTotalElements());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1,giftCertificates2.getNumberOfElements());

        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());
        assertTrue(giftCertificates2.hasContent());

        assertEquals("Gift Certificate Four", giftCertificates2.getContent().get(0).getName());
    }

    @Test
    public void shouldFindGiftCertificatesWithDateAndNameDESCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"five"});
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(tagNames,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").ascending())));
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(3,giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        assertEquals("Gift Certificate Four", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate One", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByTagNames(
                tagNames, giftCertificates.nextPageable());
        assertEquals(4, giftCertificates2.getTotalElements());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1,giftCertificates2.getNumberOfElements());

        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());
        assertTrue(giftCertificates2.hasContent());

        assertEquals("Gift Certificate Two", giftCertificates2.getContent().get(0).getName());
    }

    @Test
    public void shouldFindGiftCertificatesWithDateAndNameASCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"five"});
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                tagNames, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").ascending())));
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(3,giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        assertEquals("Gift Certificate Four", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate One", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByTagNames(
                tagNames, giftCertificates.nextPageable());
        assertEquals(4, giftCertificates2.getTotalElements());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1,giftCertificates2.getNumberOfElements());

        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());
        assertTrue(giftCertificates2.hasContent());

        assertEquals("Gift Certificate Two", giftCertificates2.getContent().get(0).getName());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificatesWithDateAndNameDESCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"nelly, old-phone"});
        Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findByTagNames(
                tagNames, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                Sort.by("created_date").and(Sort.by("name").ascending())));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesByTagNames() {
        Optional<String[]> tagNames = Optional.of(new String[]{"three"});
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByTagNames(
                tagNames, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalElements());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertEquals(2, giftCertificatePage1.getNumberOfElements());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertTrue(giftCertificatePage1.hasContent());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificatesByTagNames() {
        Optional<String[]> tagNames = Optional.of(new String[]{"nelly, old-phone"});
        Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findByTagNames(tagNames,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldThrownExceptionWhenFindGiftCertificatesByTagNames() {
        Optional<String[]> tagNames = Optional.empty();
        assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.findByTagNames(tagNames, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateSortByPartOfName() {
        Optional<String> partOfName = Optional.of("Gift");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(partOfName,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("created_date")));
        assertEquals(6, giftCertificates.getTotalElements());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(3, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("Gift Certificate One", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate Two", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByNameContaining(partOfName,
                giftCertificates.nextPageable());
        assertEquals(6, giftCertificates2.getTotalElements());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(3, giftCertificates2.getNumberOfElements());

        assertTrue(giftCertificates2.hasContent());
        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());

        assertEquals("Gift Certificate Four", giftCertificates2.getContent().get(0).getName());
        assertEquals("Gift Certificate Five", giftCertificates2.getContent().get(1).getName());
        assertEquals("Gift Certificate Six", giftCertificates2.getContent().get(2).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameASCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("Gift");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("name").ascending()));

        assertEquals(6, giftCertificates.getTotalElements());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(3, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("Gift Certificate Five", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate One", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByNameContaining(partOfName,
                giftCertificates.nextPageable());
        assertEquals(6, giftCertificates2.getTotalElements());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(3, giftCertificates2.getNumberOfElements());

        assertTrue(giftCertificates2.hasContent());
        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());

        assertEquals("Gift Certificate Six", giftCertificates2.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificates2.getContent().get(1).getName());
        assertEquals("Gift Certificate Two", giftCertificates2.getContent().get(2).getName());

    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("Gift");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName,PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("name").descending()));
        assertEquals(6, giftCertificates.getTotalElements());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(3, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("Gift Certificate Two", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Six", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByNameContaining(partOfName,
                giftCertificates.nextPageable());
        assertEquals(6, giftCertificates2.getTotalElements());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(3, giftCertificates2.getNumberOfElements());

        assertTrue(giftCertificates2.hasContent());
        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());

        assertEquals("Gift Certificate One", giftCertificates2.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificates2.getContent().get(1).getName());
        assertEquals("Gift Certificate Five", giftCertificates2.getContent().get(2).getName());

    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("Gift");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").descending())));
        assertEquals(6, giftCertificates.getTotalElements());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(3, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("Gift Certificate Two", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Six", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByNameContaining(partOfName,
                giftCertificates.nextPageable());
        assertEquals(6, giftCertificates2.getTotalElements());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(3, giftCertificates2.getNumberOfElements());

        assertTrue(giftCertificates2.hasContent());
        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());

        assertEquals("Gift Certificate One", giftCertificates2.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificates2.getContent().get(1).getName());
        assertEquals("Gift Certificate Five", giftCertificates2.getContent().get(2).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("Gift");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(partOfName,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(6, giftCertificates.getTotalElements());
        assertEquals(2, giftCertificates.getTotalPages());
        assertEquals(0, giftCertificates.getNumber());
        assertEquals(3, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("Gift Certificate One", giftCertificates.getContent().get(0).getName());
        assertEquals("Gift Certificate Two", giftCertificates.getContent().get(1).getName());
        assertEquals("Gift Certificate Three", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificates2 = giftCertificateManager.findByNameContaining(partOfName,
                giftCertificates.nextPageable());
        assertEquals(6, giftCertificates2.getTotalElements());
        assertEquals(2, giftCertificates2.getTotalPages());
        assertEquals(1, giftCertificates2.getNumber());
        assertEquals(3, giftCertificates2.getNumberOfElements());

        assertTrue(giftCertificates2.hasContent());
        assertFalse(giftCertificates2.hasNext());
        assertTrue(giftCertificates2.hasPrevious());

        assertEquals("Gift Certificate Four", giftCertificates2.getContent().get(0).getName());
        assertEquals("Gift Certificate Five", giftCertificates2.getContent().get(1).getName());
        assertEquals("Gift Certificate Six", giftCertificates2.getContent().get(2).getName());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("darken");
        Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findByNameContaining(
                partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").descending())));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfName() {
        Optional<String> partOfName = Optional.of("Gift");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByNameContaining(
                partOfName, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertNotNull(giftCertificatePage1);
        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findByNameContaining(
                partOfName, giftCertificatePage1.nextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());


        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findByNameContaining(
                partOfName, giftCertificatePage2.previousPageable());
        assertNotNull(giftCertificatePage3);
        assertEquals(6, giftCertificatePage3.getTotalElements());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertEquals(0, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());

        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificatesAndTagsByPartOfName() {
        Optional<String> partOfName = Optional.of("ten");
        Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findByNameContaining(partOfName,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfName() {
        Optional<String> partOfName = Optional.empty();
        assertThrows(GiftCertificateNotFoundException.class, ()-> {
            giftCertificateManager.findByNameContaining(partOfName,
                    PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("th");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc,
                        PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("created_date")));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Five", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertEquals("Gift Certificate Six", giftCertificatePage2.getContent().get(0).getName());


        Page<GiftCertificate> giftCertificatePage5 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage2.previousPageable());
        assertNotNull(giftCertificatePage5);
        assertEquals(4, giftCertificatePage5.getTotalElements());
        assertEquals(2, giftCertificatePage5.getTotalPages());
        assertEquals(0, giftCertificatePage5.getNumber());
        assertEquals(3, giftCertificatePage5.getNumberOfElements());

        assertTrue(giftCertificatePage5.hasContent());
        assertTrue(giftCertificatePage5.hasNext());
        assertFalse(giftCertificatePage5.hasPrevious());
        assertEquals("Gift Certificate Three", giftCertificatePage5.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage5.getContent().get(1).getName());
        assertEquals("Gift Certificate Five", giftCertificatePage5.getContent().get(2).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameASCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("th");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("created_date").ascending()));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Five", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertEquals("Gift Certificate Six", giftCertificatePage2.getContent().get(0).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("th");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, Sort.by("created_date")));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Five", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertEquals("Gift Certificate Six", giftCertificatePage2.getContent().get(0).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("th");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").descending())));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertEquals("Gift Certificate Three", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Six", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertEquals("Gift Certificate Five", giftCertificatePage2.getContent().get(0).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("th");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").ascending())));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertEquals("Gift Certificate Five", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("Gift Certificate Four", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("Gift Certificate Six", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertEquals("Gift Certificate Three", giftCertificatePage2.getContent().get(0).getName());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("android");
        Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findByDescriptionContaining(partOfDesc,
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE,
                        Sort.by("created_date").and(Sort.by("name").descending())));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("th");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, giftCertificatePage1.nextPageable());
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfDesc() {
        Optional<String> partOfDesc = Optional.empty();
        assertThrows(GiftCertificateNotFoundException.class, ()-> {
            giftCertificateManager.findByDescriptionContaining(partOfDesc,
                    PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
    }

    @Test
    public void shouldDeleteGiftCertificateById() {
        long giftCertificateId = 1L;
        GiftCertificate giftCertificate = giftCertificateManager.deleteById(giftCertificateId);
        assertNotNull(giftCertificate);
        assertEquals(giftCertificateId, giftCertificate.getId());

        assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.deleteById(giftCertificateId);
        });
    }

    @Test
    public void shouldUpdateGiftCertificateById() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("Updated Gift Certificate One");
        giftCertificate.setDescription("Gift certificate one for integration test");
        giftCertificate.setPrice(1.5);
        giftCertificate.setDuration(20);

        long giftCertificateId = 1L;
        giftCertificate.setId(giftCertificateId);
        entityManager.joinTransaction();
        GiftCertificate updatedGiftCertificate = giftCertificateManager.update(giftCertificate);
        assertGiftCertificate(giftCertificate, updatedGiftCertificate);
    }

    @Test
    public void shouldFindGiftCertificateById() {
        long giftCertificateId = 2L;
        GiftCertificate giftCertificate = giftCertificateManager.findById(giftCertificateId);
        assertNotNull(giftCertificate);
        assertEquals(giftCertificateId, giftCertificate.getId());
    }

    @Test
    public void shouldReturnEmptyWhenFindGiftCertificateById() {
        long giftCertificateId = 11L;
        assertThrows(GiftCertificateNotFoundException.class, ()-> {
            giftCertificateManager.findById(giftCertificateId);
        });
    }

    @Test
    public void shouldFindAllGiftCertificates() {
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findAll(
                PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(0, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findAll(
                giftCertificatePage1.nextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.findAll(
                giftCertificatePage2.previousPageable());
        assertNotNull(giftCertificatePage4);
        assertEquals(6, giftCertificatePage4.getTotalElements());
        assertEquals(0, giftCertificatePage4.getNumber());
        assertEquals(3, giftCertificatePage4.getNumberOfElements());
        assertEquals(2, giftCertificatePage4.getTotalPages());

        assertTrue(giftCertificatePage4.hasContent());
        assertTrue(giftCertificatePage4.hasNext());
        assertFalse(giftCertificatePage4.hasPrevious());
    }

    @Test
    public void shouldSaveGiftCertificate() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("Gift Certificate Seven");
        giftCertificate.setDescription("Seventh Gift Certificate");
        giftCertificate.setPrice(723.56);
        giftCertificate.setDuration(70);
        Optional<Tag> tagOptional = tagRepository.findByName("two");
        assertTrue(tagOptional.isPresent());
        giftCertificate.getTags().add(tagOptional.get());

        GiftCertificate savedGiftCertificate = giftCertificateManager.save(giftCertificate);
        assertGiftCertificate(giftCertificate, savedGiftCertificate);
    }

    @Test
    public void shouldFindGiftCertificatesByTagId() {
        long tagId = 3L;
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByTagId(
                tagId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(2, giftCertificatePage1.getTotalElements());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertEquals(2, giftCertificatePage1.getNumberOfElements());
        assertEquals(0, giftCertificatePage1.getNumber());

        assertTrue(giftCertificatePage1.hasContent());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyPageWhenGiftCertificatesByTagId() {
        long tagId = 10L;
        Page<GiftCertificate> giftCertificatePage = giftCertificateManager.
                findByTagId(tagId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, giftCertificatePage.getTotalElements());
    }

    private void assertGiftCertificate(GiftCertificate expectedGiftCertificate, GiftCertificate actualGiftCertificate) {
        assertEquals(expectedGiftCertificate.getId(), actualGiftCertificate.getId());
        assertEquals(expectedGiftCertificate.getName(), actualGiftCertificate.getName());
        assertEquals(expectedGiftCertificate.getDescription(), actualGiftCertificate.getDescription());
        assertEquals(expectedGiftCertificate.getPrice(), actualGiftCertificate.getPrice());
        assertEquals(expectedGiftCertificate.getDuration(), actualGiftCertificate.getDuration());
        assertEquals(expectedGiftCertificate.getCreatedDate(), actualGiftCertificate.getCreatedDate());
    }
}