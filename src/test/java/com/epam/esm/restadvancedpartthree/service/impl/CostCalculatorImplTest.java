package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.service.CostCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class CostCalculatorImplTest {
    CostCalculator costCalculator;

    @BeforeEach
    public void init() {
        costCalculator = new CostCalculatorImpl();
    }

    @Test
    public void shouldCalculateTotalCost() {
        double totalCost = costCalculator.totalCost(10, 15.5);
        assertEquals(155.0, totalCost);
    }

    @Test
    public void shouldCalculateZeroTotalCost1() {
        double totalCost = costCalculator.totalCost(0, 15.5);
        assertEquals(0.0, totalCost);
    }

    @Test
    public void shouldCalculateZeroTotalCost2() {
        double totalCost = costCalculator.totalCost(1, 0);
        assertEquals(0.0, totalCost);
    }

    @Test
    public void shouldCalculateZeroTotalCost3() {
        double totalCost = costCalculator.totalCost(0, 0);
        assertEquals(0.0, totalCost);
    }
}