package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.GiftCertificateRepository;
import com.epam.esm.restadvancedpartthree.repository.OrderRepository;
import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import com.epam.esm.restadvancedpartthree.repository.entity.OrderStatus;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import com.epam.esm.restadvancedpartthree.service.CostCalculator;
import com.epam.esm.restadvancedpartthree.service.GiftCertificateManager;
import com.epam.esm.restadvancedpartthree.service.OrderManager;
import com.epam.esm.restadvancedpartthree.service.UserManager;
import com.epam.esm.restadvancedpartthree.service.exception.OrderNotFoundException;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class OrderManagerImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    Environment environment;
    @Autowired
    OrderRepository orderRepository;
    OrderManager orderManager;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    GiftCertificateManager giftCertificateManager;
    @Autowired
    UserRepository userRepository;
    UserManager userManager;
    @Autowired
    TagRepository tagRepository;
    @Mock
    CostCalculator costCalculator;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void init() {
        giftCertificateManager = new GiftCertificateManagerImpl(giftCertificateRepository, tagRepository, environment);
        userManager = new UserManagerImpl(userRepository, environment);
        orderManager = new OrderManagerImpl(orderRepository, costCalculator, environment);
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveOrder() {
        long giftCertificateId = 2L;
        GiftCertificate giftCertificate = giftCertificateManager.findById(giftCertificateId);
        long userId = 1L;
        User user = userManager.findById(userId);

        int quantity = 10;
        when(costCalculator.totalCost(quantity, giftCertificate.getPrice())).
                thenReturn(quantity * giftCertificate.getPrice());
        Order order = new Order(quantity, OrderStatus.UNPAID, user, giftCertificate);
        Order saveOrder = orderManager.save(order);

        assertOrder(order, saveOrder);
        verify(costCalculator, VerificationModeFactory.times(1)).
                totalCost(quantity, giftCertificate.getPrice());
    }

    @Test
    public void shouldFindOrderById() {
        long orderId = 3L;
        Order order = orderManager.findById(orderId);
        assertEquals(orderId, order.getId());
    }

    @Test
    public void shouldThrowExceptionWhenFindOrderById() {
        long orderId = 7L;
        assertThrows(OrderNotFoundException.class, () -> {
            orderManager.findById(orderId);
        });
    }

    @Test
    public void findOrdersByGiftCertificateId() {
        long giftCertificateId = 1L;

        Page<Order> orderPage = orderManager.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(0, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificateId() {
        long giftCertificateId = 4L;
        Page<Order> orderPage = orderManager.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, orderPage.getTotalElements());
    }

    @Test
    public void shouldFindOrdersByOwnerId() {
        long userId = 2L;
        Page<Order> orderPage = orderManager.findByOwnerId(
                userId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(0, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyWhenFindOwnerId() {
        long ownerId = 5L;
        Page<Order> orderPage = orderManager.findByOwnerId(
                ownerId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, orderPage.getTotalElements());
    }

    @Test
    public void shouldDeleteOrder() {
        long orderId = 1L;
        Order order = orderManager.deleteById(orderId);
        assertEquals(orderId, order.getId());
    }

    @Test
    @DisplayName("throw exception when delete order")
    public void shouldThrowExceptionWhenDeleteOrder() {
        long orderId = 7L;
        assertThrows(OrderNotFoundException.class, () -> {
            orderManager.deleteById(orderId);
        });
    }

    private void assertOrder(Order expectedOrder, Order actualOrder) {
        assertEquals(expectedOrder.getId(), actualOrder.getId());
        assertEquals(expectedOrder.getQuantity(), actualOrder.getQuantity());
        assertEquals(expectedOrder.getStatus(), actualOrder.getStatus());
        assertEquals(expectedOrder.getTotalCost(), actualOrder.getTotalCost());
        assertEquals(expectedOrder.getOwner().getId(), actualOrder.getOwner().getId());
        assertEquals(expectedOrder.getGiftCertificate().getId(), actualOrder.getGiftCertificate().getId());
        assertEquals(expectedOrder.getOrderedDate(), actualOrder.getOrderedDate());
    }
}