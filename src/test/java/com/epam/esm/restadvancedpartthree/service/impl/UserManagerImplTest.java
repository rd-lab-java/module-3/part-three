package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.config.TestMysqlConfig;
import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import com.epam.esm.restadvancedpartthree.service.UserManager;
import com.epam.esm.restadvancedpartthree.service.exception.UserNotFoundException;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
class UserManagerImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    Environment environment;
    @Autowired
    UserRepository userRepository;
    UserManager userManager;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void init() {
        userManager = new UserManagerImpl(userRepository, environment);
    }

    @Test
    public void shouldSaveUser() {
        User user = new User();
        user.setFullName("Martin King");
        user.setEmail("marin_king@email.com");
        user.setPassword("martin_king");
        User saveUser = userManager.save(user);
        assertUser(user, saveUser);
    }


    @Test
    public void shouldFindAll() {
        Page<User> userPage = userManager.findAll(PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(5, userPage.getTotalElements());
        assertEquals(2, userPage.getTotalPages());
        assertEquals(3, userPage.getNumberOfElements());
        assertEquals(0, userPage.getNumber());

        assertTrue(userPage.hasContent());
        assertTrue(userPage.hasNext());
        assertFalse(userPage.hasPrevious());

        Page<User> userPage2 = userManager.findAll(userPage.nextPageable());
        assertEquals(5, userPage2.getTotalElements());
        assertEquals(2, userPage2.getTotalPages());
        assertEquals(2, userPage2.getNumberOfElements());
        assertEquals(1, userPage2.getNumber());

        assertTrue(userPage2.hasContent());
        assertFalse(userPage2.hasNext());
        assertTrue(userPage2.hasPrevious());

        Page<User> userPage3 = userManager.findAll(userPage2.previousPageable());
        assertEquals(5, userPage3.getTotalElements());
        assertEquals(2, userPage3.getTotalPages());
        assertEquals(3, userPage3.getNumberOfElements());
        assertEquals(0, userPage3.getNumber());

        assertTrue(userPage3.hasContent());
        assertTrue(userPage3.hasNext());
        assertFalse(userPage3.hasPrevious());
    }


    @Test
    @DisplayName("find user by id")
    public void shouldFindUserById() {
        long userId = 2L;
        User user = userManager.findById(userId);
        assertEquals(userId, user.getId());
    }

    @Test
    @DisplayName("throw exception when find user by id")
    public void shouldThrowExceptionWhenFindUserById() {
        long userId = 6L;
        assertThrows(UserNotFoundException.class, ()-> {
            userManager.findById(userId);
        });
    }

    @Test
    public void shouldDeleteUserById() {
        long userId = 1L;
        User user = userManager.deleteById(userId);
        assertEquals(userId, user.getId());

        assertThrows(UserNotFoundException.class, () -> {
            userManager.deleteById(userId);
        });
    }

    @Test
    public void shouldThrowExceptionWhenDeleteUser() {
        long userId = 6L;
        assertThrows(UserNotFoundException.class, () -> {
            userManager.deleteById(userId);
        });
    }

    private void assertUser(User expectedUser, User actualUser) {
        assertEquals(expectedUser.getId(), actualUser.getId());
        assertEquals(expectedUser.getFullName(), actualUser.getFullName());
        assertEquals(expectedUser.getEmail(), actualUser.getEmail());
        assertEquals(expectedUser.getPassword(), actualUser.getPassword());
        assertEquals(expectedUser.getCreatedDate(), actualUser.getCreatedDate());
    }
}