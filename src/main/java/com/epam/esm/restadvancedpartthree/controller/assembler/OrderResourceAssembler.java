package com.epam.esm.restadvancedpartthree.controller.assembler;

import com.epam.esm.restadvancedpartthree.controller.v1.GiftCertificateController;
import com.epam.esm.restadvancedpartthree.controller.v1.OrderController;
import com.epam.esm.restadvancedpartthree.controller.v1.UserController;
import com.epam.esm.restadvancedpartthree.controller.model.GiftCertificateResource;
import com.epam.esm.restadvancedpartthree.controller.model.OrderResource;
import com.epam.esm.restadvancedpartthree.controller.model.UserResource;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class OrderResourceAssembler {

    @Autowired
    private UserResourceAssembler userResourceAssembler;
    @Autowired
    private GiftCertificateResourceAssembler giftCertificateResourceAssembler;

    public OrderResource toResource(Order order) {
        UserResource userResource = userResourceAssembler.toResource(order.getOwner());
        GiftCertificateResource giftCertificateResource = giftCertificateResourceAssembler.
                toResource(order.getGiftCertificate());
        OrderResource orderResource = new OrderResource(order.getId(), order.getTotalCost(), order.getQuantity(),
                order.getOrderedDate(), order.getUnitPrice(), userResource, giftCertificateResource);

        orderResource.add(linkTo(methodOn(OrderController.class).findOrderById(order.getId())).withSelfRel());
        orderResource.add(linkTo(methodOn(UserController.class).findUserById(userResource.id)).withRel("owner"));
        orderResource.add(linkTo(methodOn(GiftCertificateController.class).
                findGiftCertificateById(giftCertificateResource.id)).withRel("giftCertificate"));

        return orderResource;
    }
}
