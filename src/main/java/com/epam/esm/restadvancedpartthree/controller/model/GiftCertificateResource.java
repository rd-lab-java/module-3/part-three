package com.epam.esm.restadvancedpartthree.controller.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.RepresentationModel;
import java.util.Date;

public class GiftCertificateResource extends RepresentationModel<GiftCertificateResource> {
    public long id;
    public String name;
    public String description;
    public double price;
    public int duration;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date createdDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date updatedDate;

    public GiftCertificateResource(long id, String name,
                                   String description,
                                   double price,
                                   int duration,
                                   Date createdDate,
                                   Date updatedDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }
}
