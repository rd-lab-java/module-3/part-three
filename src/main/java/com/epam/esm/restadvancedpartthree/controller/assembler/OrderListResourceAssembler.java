package com.epam.esm.restadvancedpartthree.controller.assembler;

import com.epam.esm.restadvancedpartthree.common.api.resource.PageLinks;
import com.epam.esm.restadvancedpartthree.controller.v1.OrderController;
import com.epam.esm.restadvancedpartthree.controller.model.OrderListResource;
import com.epam.esm.restadvancedpartthree.controller.model.OrderResource;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderListResourceAssembler {
    @Autowired
    private OrderResourceAssembler orderResourceAssembler;

    @PageLinks(OrderController.class)
    public OrderListResource build(Page<Order> page) {
        List<OrderResource> orderResources = page.getContent()
                .stream()
                .map(order -> orderResourceAssembler.toResource(order))
                .collect(Collectors.toList());

        return new OrderListResource(orderResources, page.getNumber(), page.getSize(),
                page.getTotalPages(), page.getTotalElements());
    }
}
