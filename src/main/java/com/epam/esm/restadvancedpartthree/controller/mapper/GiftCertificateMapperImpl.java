package com.epam.esm.restadvancedpartthree.controller.mapper;

import com.epam.esm.restadvancedpartthree.controller.model.GiftCertificateResourceInput;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import org.springframework.stereotype.Component;

@Component
public class GiftCertificateMapperImpl implements GiftCertificateMapper {
    @Override
    public GiftCertificateResourceInput asGiftCertificateResourceInput(GiftCertificate giftCertificate) {
        return new GiftCertificateResourceInput(giftCertificate.getId(), giftCertificate.getName(),
                giftCertificate.getDescription(), giftCertificate.getPrice(), giftCertificate.getDuration());
    }

    @Override
    public void update(GiftCertificateResourceInput giftCertificateResource, GiftCertificate giftCertificate) {
        giftCertificate.setId(giftCertificateResource.getId());
        giftCertificate.setName(giftCertificateResource.getName());
        giftCertificate.setDescription(giftCertificateResource.getDescription());
        giftCertificate.setPrice(giftCertificateResource.getPrice());
        giftCertificate.setDuration(giftCertificateResource.getDuration());
    }
}
