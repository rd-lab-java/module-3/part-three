package com.epam.esm.restadvancedpartthree.controller.assembler;

import com.epam.esm.restadvancedpartthree.common.api.resource.PageLinks;
import com.epam.esm.restadvancedpartthree.controller.v1.GiftCertificateController;
import com.epam.esm.restadvancedpartthree.controller.model.GiftCertificateListResource;
import com.epam.esm.restadvancedpartthree.controller.model.GiftCertificateResource;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GiftCertificateListResourceAssembler {

    @Autowired
    private GiftCertificateResourceAssembler giftCertificateResourceAssembler;

    @PageLinks(GiftCertificateController.class)
    public GiftCertificateListResource build(Page<GiftCertificate> page) {
        List<GiftCertificateResource> giftCertificates = page.getContent()
                .stream()
                .map(giftCertificate -> giftCertificateResourceAssembler.toResource(giftCertificate))
                .collect(Collectors.toList());

        return new GiftCertificateListResource(giftCertificates, page.getNumber(), page.getSize(),
                page.getTotalPages(), page.getTotalElements());
    }
}
