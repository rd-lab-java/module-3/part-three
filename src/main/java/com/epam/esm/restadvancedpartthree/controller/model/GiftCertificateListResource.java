package com.epam.esm.restadvancedpartthree.controller.model;

import com.epam.esm.restadvancedpartthree.common.api.resource.AbstractListResource;

import java.util.Collection;

public class GiftCertificateListResource extends AbstractListResource {
    private final Collection<GiftCertificateResource> giftCertificates;

    /**
     * Creates new instance of {@link AbstractListResource}.
     *
     * @param pageNumber    actual page of the collection.
     * @param pageSize      number of elements per pages of the collection.
     * @param totalPages    total number of pages of the collection.
     * @param totalElements total number of elements of the collection.
     */
    public GiftCertificateListResource(Collection<GiftCertificateResource> giftCertificates, int pageNumber,
                                       int pageSize, int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.giftCertificates = giftCertificates;
    }

    public Collection<GiftCertificateResource> getGiftCertificates() {
        return giftCertificates;
    }
}
