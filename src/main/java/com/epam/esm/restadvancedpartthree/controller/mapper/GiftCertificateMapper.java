package com.epam.esm.restadvancedpartthree.controller.mapper;

import com.epam.esm.restadvancedpartthree.controller.model.GiftCertificateResourceInput;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;

public interface GiftCertificateMapper {
    GiftCertificateResourceInput asGiftCertificateResourceInput(GiftCertificate giftCertificate);
    void update(GiftCertificateResourceInput giftCertificateResource, GiftCertificate giftCertificate);
}
