package com.epam.esm.restadvancedpartthree.controller.v1;

import com.epam.esm.restadvancedpartthree.controller.assembler.OrderResourceAssembler;
import com.epam.esm.restadvancedpartthree.controller.model.OrderResource;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import com.epam.esm.restadvancedpartthree.service.OrderManager;
import com.epam.esm.restadvancedpartthree.service.exception.OrderNotFoundException;
import com.epam.esm.restadvancedpartthree.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Rest api controller for order
 */
@RestController
@RequestMapping("/v1/orders")
public class OrderController {
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private OrderResourceAssembler orderResourceAssembler;

    /**
     * Find order by id
     * @param orderId order id that is to be found
     * @return The common response {@link ResponseUtils} containing status code,
     *  message and actual data ({@link Order})
     * @throws RuntimeException {@link OrderNotFoundException} when no any data based on order id.
     */
    @GetMapping("/{orderId:[0-9]+}")
    public ResponseUtils findOrderById(@PathVariable long orderId) {
        Order order = orderManager.findById(orderId);
        OrderResource orderResource = orderResourceAssembler.toResource(order);
        return ResponseUtils.success(orderResource);
    }

    /**
     * Delete order by id
     * @param orderId id of an Order that is to be deleted
     * @return The common response {@link ResponseUtils} containing status code,
     *  message and actual data ({@link Order})
     * @throws RuntimeException {@link OrderNotFoundException} when no any data based on order id.
     */
    @DeleteMapping("/{orderId:[0-9]+}")
    public ResponseUtils deleteOrderById(@PathVariable long orderId) {
        Order deletedOrder = orderManager.deleteById(orderId);
        return ResponseUtils.response(200, "Order deleted successfully",
                orderResourceAssembler.toResource(deletedOrder));
    }
}
