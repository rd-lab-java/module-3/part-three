package com.epam.esm.restadvancedpartthree.controller.assembler;

import com.epam.esm.restadvancedpartthree.common.api.resource.PageLinks;
import com.epam.esm.restadvancedpartthree.controller.v1.TagController;
import com.epam.esm.restadvancedpartthree.controller.model.TagListResource;
import com.epam.esm.restadvancedpartthree.controller.model.TagResource;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TagListResourceAssembler {
    @Autowired
    private TagResourceAssembler tagResourceAssembler;

    @PageLinks(TagController.class)
    public TagListResource build(Page<Tag> page) {
        List<TagResource> tags = page.getContent()
                .stream()
                .map(tag -> tagResourceAssembler.toResource(tag))
                .collect(Collectors.toList());
        return new TagListResource(tags, page.getNumber(), page.getSize(), page.getTotalPages(),
                page.getTotalElements());
    }
}
