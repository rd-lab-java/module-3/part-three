package com.epam.esm.restadvancedpartthree.controller.common;

import javax.json.JsonMergePatch;

public interface PatchHelper {
    <T> T mergePatch(JsonMergePatch mergePatch, T targetBean, Class<T> beanClass);
}
