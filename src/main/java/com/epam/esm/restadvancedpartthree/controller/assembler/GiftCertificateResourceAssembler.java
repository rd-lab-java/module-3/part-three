package com.epam.esm.restadvancedpartthree.controller.assembler;

import com.epam.esm.restadvancedpartthree.controller.v1.GiftCertificateController;
import com.epam.esm.restadvancedpartthree.controller.model.GiftCertificateResource;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class GiftCertificateResourceAssembler {
    public GiftCertificateResource toResource(GiftCertificate giftCertificate) {
        GiftCertificateResource resource = new GiftCertificateResource(giftCertificate.getId(), giftCertificate.getName(),
                giftCertificate.getDescription(), giftCertificate.getPrice(), giftCertificate.getDuration(),
                giftCertificate.getCreatedDate(), giftCertificate.getUpdatedDate());
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateById(giftCertificate.getId()))
                .withSelfRel());
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateTags(giftCertificate.getId(),
                Pageable.unpaged())).withRel("tags"));
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateOrders(giftCertificate.getId(),
                Pageable.unpaged())).withRel("orders"));
        
        return resource;
    }
}
