package com.epam.esm.restadvancedpartthree.controller.model;

import com.epam.esm.restadvancedpartthree.common.api.resource.AbstractListResource;

import java.util.Collection;

public class TagListResource extends AbstractListResource {
    private final Collection<TagResource> tags;

    public TagListResource(Collection<TagResource> tags, int pageNumber, int pageSize, int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.tags = tags;
    }

    public Collection<TagResource> getTags() {
        return tags;
    }
}
