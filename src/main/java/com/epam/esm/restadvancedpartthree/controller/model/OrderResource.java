package com.epam.esm.restadvancedpartthree.controller.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;

public class OrderResource extends RepresentationModel<OrderResource> {
    public long id;
    public double totalCost;
    public int quantity;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date orderedDate;
    public double unitPrice;
    public UserResource owner;
    public GiftCertificateResource giftCertificate;

    public OrderResource(long id, double totalCost, int quantity, Date orderedDate, double unitPrice,
                         UserResource owner, GiftCertificateResource giftCertificate) {
        this.id = id;
        this.totalCost = totalCost;
        this.quantity = quantity;
        this.orderedDate = orderedDate;
        this.unitPrice = unitPrice;
        this.owner = owner;
        this.giftCertificate = giftCertificate;
    }
}
