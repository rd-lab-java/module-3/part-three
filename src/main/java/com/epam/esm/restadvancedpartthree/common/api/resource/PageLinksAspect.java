package com.epam.esm.restadvancedpartthree.common.api.resource;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Map;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;


/**
 * Implementation of the {@PageLinks} annotation to generate HATEOAS Links for paged list resources.
 */
@Component
@Aspect
public class PageLinksAspect {
    @Around("@annotation(com.epam.esm.restadvancedpartthree.common.api.resource.PageLinks) && " +
            "execution(org.springframework.hateoas.RepresentationModel+ *(..)) && args(page, ..)")
    public Object pageLinksAdvice(ProceedingJoinPoint joinPoint, Page<?> page) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.
                getRequestAttributes()).getRequest();
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        PageLinks pageLinks = method.getAnnotation(PageLinks.class);
        Class<?> controller = pageLinks.value();
        UriComponentsBuilder original = originalUri(controller, request);
        RepresentationModel representationModel = (RepresentationModel) joinPoint.proceed();

        if(!page.isFirst() && !page.isEmpty()) {
            UriComponentsBuilder firstBuilder = replacePageParams(original,
                    PageRequest.of(0, page.getSize(), page.getSort()));
            representationModel.add(Link.of(firstBuilder.toUriString()).withRel("first"));
        }

        if (page.hasNext()) {
            UriComponentsBuilder nextBuilder = replacePageParams(original, page.nextPageable());
            representationModel.add(Link.of(nextBuilder.toUriString()).withRel("next"));
        }

        if (page.hasPrevious()) {
            UriComponentsBuilder prevBuilder = replacePageParams(original, page.previousPageable());
            representationModel.add(Link.of(prevBuilder.toUriString()).withRel("prev"));
        }


        if(!page.isLast() && !page.isEmpty()) {
            UriComponentsBuilder lastBuilder = replacePageParams(original,
                    PageRequest.of(page.getTotalPages() - 1, page.getSize(), page.getSort()));
            representationModel.add(Link.of(lastBuilder.toUriString()).withRel("last"));
        }

        return representationModel;
    }

    private UriComponentsBuilder originalUri(Class<?> controller, HttpServletRequest request) {
        UriComponentsBuilder baseUri = linkTo(controller).toUriComponentsBuilder();
        String relativePath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String contextPath = request.getContextPath();
        baseUri.replacePath(contextPath + relativePath);
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            for (String value : entry.getValue()) {
                baseUri.queryParam(entry.getKey(), value);
            }
        }
        return baseUri;
    }

    private UriComponentsBuilder replacePageParams(UriComponentsBuilder original, Pageable pageable) {
        UriComponentsBuilder builder = original.cloneBuilder();
        builder.replaceQueryParam("page", pageable.getPageNumber());
        builder.replaceQueryParam("size", pageable.getPageSize());
        return builder;
    }
}
