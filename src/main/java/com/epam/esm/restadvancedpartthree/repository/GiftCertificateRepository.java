package com.epam.esm.restadvancedpartthree.repository;

import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Gift Certificate repository interface 
 * imposes basic operations that working with data source.
 * @author yusuf
 *
 */
@Repository
public interface GiftCertificateRepository extends JpaRepository<GiftCertificate, Long> {
    @Query(value = "select * from gift_certificate where id in " +
            "(select gift_certs_tags.gift_certificate_id from gift_certs_tags where gift_certs_tags.tag_id in " +
            "(select tag.id from tag where tag.name in (:tagNames)) group by gift_certs_tags.gift_certificate_id " +
            "having count(gift_certs_tags.gift_certificate_id) = :tagNamesCount)", nativeQuery = true,
    countQuery = "select count(*) from gift_certificate where id in " +
            "(select gift_certs_tags.gift_certificate_id from gift_certs_tags where gift_certs_tags.tag_id in " +
            "(select tag.id from tag where tag.name in (:tagNames)) group by gift_certs_tags.gift_certificate_id " +
            "having count(gift_certs_tags.gift_certificate_id) = :tagNamesCount);")
    Page<GiftCertificate> findByTagNames(
            @Param("tagNames") String[] tagNames, @Param("tagNamesCount") int tagNamesCount, Pageable pageable);

    @Query(value = "select * from gift_certificate where name like %:name%", nativeQuery = true,
    countQuery = "select count(*) from gift_certificate where name like %:name%")
    Page<GiftCertificate> findByNameContaining(@Param("name") String name, Pageable pageable);

    @Query(value = "select * from gift_certificate where description like %:description%", nativeQuery = true,
    countQuery = "select count(*) from gift_certificate where description like %:description%")
    Page<GiftCertificate> findByDescriptionContaining(@Param("description") String description, Pageable pageable);

    @Query(value = "select * from gift_certificate join gift_certs_tags on gift_certificate.id = " +
            "gift_certs_tags.gift_certificate_id where gift_certs_tags.tag_id = :tagId", nativeQuery = true,
            countQuery = "select count(*) from gift_certificate join gift_certs_tags on gift_certificate.id = " +
                    "gift_certs_tags.gift_certificate_id where gift_certs_tags.tag_id = :tagId")
    Page<GiftCertificate> findByTagId(@Param("tagId") long tagId, Pageable pageable);

    @Modifying
    @Query(value = "update gift_certificate set name=:name, description=:description, price=:price, " +
            "duration=:duration, last_updated_date=:updatedDate where id=:id", nativeQuery = true)
    void updateById(@Param("id") long id, @Param("name") String name, @Param("description") String description,
                    @Param("price") double price, @Param("duration") int duration,
                    @Param("updatedDate") Date updatedDate);

    @Modifying
    @Query(value = "insert ignore into gift_certs_tags(gift_certificate_id, tag_id) " +
            "values(:gift_certificate_id, :tag_id)", nativeQuery = true)
    void saveGiftCertificateAndTag(
            @Param("gift_certificate_id") long gift_certificate_id, @Param("tag_id") long tag_id);
}
