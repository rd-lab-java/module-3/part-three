package com.epam.esm.restadvancedpartthree.repository.entity;


import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "gift_certificate_order",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"owner_id", "gift_certificate_id"})
        },
        indexes = {
            @Index(name = "total_cost_idx", columnList = "total_cost")
        }
)
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq")
    @SequenceGenerator(name = "order_seq", sequenceName = "order_seq", initialValue = 1, allocationSize = 10)
    private long id;

    @Column(name = "total_cost")
    private double totalCost;

    @Column(name = "unit_price")
    private double unitPrice;

    private int quantity;

    @Convert(converter = OrderStatusConverter.class)
    private OrderStatus status;

    @CreatedDate
    @Column(name = "ordered_date", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderedDate;

    @LastModifiedDate
    @Column(name = "last_updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdatedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gift_certificate_id", nullable = false)
    private GiftCertificate giftCertificate;

    public Order() {
        this.orderedDate = new Date();
    }

    public Order(int quantity, OrderStatus status, User owner, GiftCertificate giftCertificate) {
        this.quantity = quantity;
        this.status = status;
        this.owner = owner;
        this.giftCertificate = giftCertificate;
        this.unitPrice = giftCertificate.getPrice();
    }
}
