package com.epam.esm.restadvancedpartthree.repository;

import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Tag repository interface
 * imposes basic CRUD operations 
 * that working directly with data source
 * 
 * @author yusuf
 *
 */
@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    @Modifying
    @Query(value = "alter table tag_seq alter next_val restart with 1;", nativeQuery = true)
    void resetAutoIncrement();

    Optional<Tag> findByName(String name);

    @Query(value = "select * from tag join gift_certs_tags on tag.id = gift_certs_tags.tag_id " +
            "where gift_certs_tags.gift_certificate_id = :giftCertificateId", nativeQuery = true,
        countQuery = "select count(*) from tag join gift_certs_tags on tag.id = gift_certs_tags.tag_id " +
                "where gift_certs_tags.gift_certificate_id=:giftCertificateId")
    Page<Tag> findByGiftCertificateId(@Param("giftCertificateId") long giftCertificateId, Pageable pageable);

    @Query(value = "select distinct t.* from tag t " +
            "left join gift_certs_tags gct on gct.tag_id = t.id " +
            "left join gift_certificate_order ogc on ogc.gift_certificate_id = gct.gift_certificate_id " +
            "right join (select distinct ogct.total_cost from gift_certificate_order ogct " +
            "order by ogct.total_cost desc) n on ogc.total_cost = n.total_cost",
            nativeQuery = true, countQuery = "select count(distinct t.id) from tag t " +
            "left join gift_certs_tags gct on gct.tag_id = t.id " +
            "left join gift_certificate_order ogc on ogc.gift_certificate_id = gct.gift_certificate_id " +
            "right join (select distinct ogct.total_cost from gift_certificate_order ogct " +
            "order by ogct.total_cost desc) n on ogc.total_cost = n.total_cost")
    Page<Tag> findMostUsedTags(Pageable pageable);

    @Query(value = "select distinct t.* from tag t " +
            "left join gift_certs_tags gct on gct.tag_id = t.id " +
            "left join gift_certificate_order ogc on ogc.gift_certificate_id = gct.gift_certificate_id " +
            "right join (select distinct ogct.total_cost from gift_certificate_order ogct " +
            "order by ogct.total_cost desc) n on ogc.total_cost = n.total_cost limit :number", nativeQuery = true)
    List<Tag> findTopNumberMostUsedTags(@Param("number") int number);
}
