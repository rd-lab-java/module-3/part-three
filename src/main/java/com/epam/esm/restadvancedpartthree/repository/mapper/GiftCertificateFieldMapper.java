package com.epam.esm.restadvancedpartthree.repository.mapper;

import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;

public interface GiftCertificateFieldMapper {
    String retrieveChangedFields(GiftCertificate original, GiftCertificate another);
}
