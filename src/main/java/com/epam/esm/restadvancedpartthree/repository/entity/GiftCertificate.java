package com.epam.esm.restadvancedpartthree.repository.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

@Data
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "gift_certificate")
@JsonIgnoreProperties(value = {"tags", "orders"}, allowSetters = true)
public class GiftCertificate {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gift_certificate_seq")
	@SequenceGenerator(name = "gift_certificate_seq", sequenceName = "gift_certificate_seq",
			initialValue = 1 , allocationSize = 10)
    private long id;
	@Column(nullable = false, unique = true)
    private String name;
    private String description;
    @Column(nullable = false)
    private double price;
    @Column(nullable = false)
    private int duration;
    @CreatedDate
    @Column(name = "created_date", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @LastModifiedDate
    @Column(name = "last_updated_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "gift_certs_tags",
			joinColumns = {@JoinColumn(name = "gift_certificate_id")},
			inverseJoinColumns = {@JoinColumn(name = "tag_id")},
			uniqueConstraints = @UniqueConstraint(columnNames = {"gift_certificate_id", "tag_id"}),
			indexes = {@Index(name = "gift_certificate_id_idx", columnList = "gift_certificate_id")})
	private Collection<Tag> tags = new HashSet<>();
	@OneToMany(mappedBy = "giftCertificate", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Collection<Order> orders = new HashSet<>();

	public GiftCertificate(String name, String description, double price, int duration) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.duration = duration;
	}
	
	public GiftCertificate() {}
}
