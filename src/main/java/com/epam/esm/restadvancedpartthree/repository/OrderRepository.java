package com.epam.esm.restadvancedpartthree.repository;

import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("select o from Order o join fetch o.giftCertificate gc join fetch o.owner ow where o.id=:id")
    Optional<Order> findById(@Param("id") long id);

    @Modifying
    @Query(value = "alter table order_seq alter next_val restart with 1;", nativeQuery = true)
    void resetAutoIncrement();
    Page<Order> findByGiftCertificateId(long giftCertificateId, Pageable pageable);
    Page<Order> findByOwnerId(long ownerId, Pageable pageable);
}
