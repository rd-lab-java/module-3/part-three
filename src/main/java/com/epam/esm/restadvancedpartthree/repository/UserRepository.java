package com.epam.esm.restadvancedpartthree.repository;

import com.epam.esm.restadvancedpartthree.repository.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Modifying
    @Query(value = "alter table user_seq alter next_val restart with 1", nativeQuery = true)
    void resetAutoIncrement();
}
