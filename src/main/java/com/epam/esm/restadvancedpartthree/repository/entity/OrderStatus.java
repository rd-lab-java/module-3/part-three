package com.epam.esm.restadvancedpartthree.repository.entity;

import com.epam.esm.restadvancedpartthree.service.exception.IllegalOrderStatusValueException;

public enum OrderStatus {
    PAID("paid"), UNPAID("unpaid"), CANCELLED("cancelled"), DONE("done");

    private final String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static OrderStatus fromValue(String value) throws IllegalOrderStatusValueException {
        for (OrderStatus status : values()) {
            if (status.getValue().equals(value)) {
                return status;
            }
        }

        throw new IllegalOrderStatusValueException(value);
    }
}
