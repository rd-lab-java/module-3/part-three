package com.epam.esm.restadvancedpartthree.data.job.processor;

import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.springframework.batch.item.ItemProcessor;

public class TagItemProcessor implements ItemProcessor<String, Tag> {
    public TagItemProcessor() {
    }

    @Override
    public Tag process(String item) throws Exception {
        Tag tag = new Tag();
        tag.setName(item);
        return tag;
    }
}
