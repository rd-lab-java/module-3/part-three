package com.epam.esm.restadvancedpartthree.data.job.processor;

import com.epam.esm.restadvancedpartthree.data.job.service.RandomNumberGenerator;
import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import com.epam.esm.restadvancedpartthree.repository.entity.OrderStatus;
import com.epam.esm.restadvancedpartthree.service.exception.IllegalOrderStatusValueException;
import org.springframework.batch.item.ItemProcessor;

public class OrderItemProcessor implements ItemProcessor<GiftCertificate, Order> {
    private final UserRepository userRepository;
    private final RandomNumberGenerator numberGenerator;
    public OrderItemProcessor(UserRepository userRepository, RandomNumberGenerator numberGenerator) {
        this.userRepository = userRepository;
        this.numberGenerator = numberGenerator;
    }

    @Override
    public Order process(GiftCertificate item) throws Exception {
        Order order = new Order();
        order.setGiftCertificate(item);
        int quantity = numberGenerator.randomNumber(1, 10);
        order.setQuantity(quantity);
        order.setStatus(getStatus());
        order.setUnitPrice(item.getPrice());
        order.setTotalCost(quantity * item.getPrice());
        userRepository.findById((long)numberGenerator.randomNumber(1, 1000)).ifPresent(order::setOwner);
        return order;
    }

    private OrderStatus getStatus() throws IllegalOrderStatusValueException {
        switch (numberGenerator.randomNumber(1, 4)) {
            case 1:
                return OrderStatus.UNPAID;
            case 2:
                return OrderStatus.PAID;
            case 3:
                return OrderStatus.CANCELLED;
            case 4:
                return OrderStatus.DONE;
            default:
                throw new IllegalOrderStatusValueException("");
        }
    }
}
