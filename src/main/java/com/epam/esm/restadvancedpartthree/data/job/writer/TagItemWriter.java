package com.epam.esm.restadvancedpartthree.data.job.writer;

import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class TagItemWriter implements ItemWriter<Tag> {
    private final TagRepository tagRepository;
    public TagItemWriter(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public void write(List<? extends Tag> items) throws Exception {
        tagRepository.saveAll(items);
    }
}
