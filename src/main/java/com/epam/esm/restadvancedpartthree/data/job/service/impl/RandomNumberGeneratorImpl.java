package com.epam.esm.restadvancedpartthree.data.job.service.impl;

import com.epam.esm.restadvancedpartthree.data.job.service.RandomNumberGenerator;
import org.springframework.stereotype.Service;

@Service
public class RandomNumberGeneratorImpl implements RandomNumberGenerator {
    @Override
    public int randomNumber(int min, int max) {
        return ((int) (Math.random() * (max - min)) + min);
    }

    @Override
    public double randomNumber(double min, double max) {
        return ((Math.random() * (max - min)) + min);
    }
}
