package com.epam.esm.restadvancedpartthree.data.job.processor;

import com.epam.esm.restadvancedpartthree.data.job.model.UserCredential;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import org.springframework.batch.item.ItemProcessor;

public class UserItemProcessor implements ItemProcessor<UserCredential, User> {

    @Override
    public User process(UserCredential item) throws Exception {
        User user = new User();
        user.setFullName(item.getFirstName() + ", " + item.getLastName());
        user.setEmail(item.getEmail());
        user.setPassword(item.getFirstName().toLowerCase() + "_" + item.getLastName().toLowerCase());
        return user;
    }
}
