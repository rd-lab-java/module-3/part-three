package com.epam.esm.restadvancedpartthree.data.job.mapper;

import com.epam.esm.restadvancedpartthree.data.job.model.UserCredential;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class UserCredentialSetMapper implements FieldSetMapper<UserCredential> {
    @Override
    public UserCredential mapFieldSet(FieldSet fieldSet) throws BindException {
        UserCredential userCredential = new UserCredential();
        userCredential.setEmail(fieldSet.readString("email"));
        userCredential.setFirstName(fieldSet.readString("first_name"));
        userCredential.setLastName(fieldSet.readString("last_name"));
        return userCredential;
    }
}
