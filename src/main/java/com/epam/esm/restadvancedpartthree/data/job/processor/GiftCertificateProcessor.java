package com.epam.esm.restadvancedpartthree.data.job.processor;

import com.epam.esm.restadvancedpartthree.data.job.service.RandomNumberGenerator;
import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import org.springframework.batch.item.ItemProcessor;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class GiftCertificateProcessor implements ItemProcessor<String, GiftCertificate> {
    private final TagRepository tagRepository;
    private final RandomNumberGenerator numberGenerator;
    public GiftCertificateProcessor(TagRepository tagRepository, RandomNumberGenerator numberGenerator) {
        this.tagRepository = tagRepository;
        this.numberGenerator = numberGenerator;
    }

    @Override
    public GiftCertificate process(String item) throws Exception {
        int itemLength = item.trim().length();
        if(itemLength == 0 || itemLength == 1) {
            return null;
        }
        int tagSize = numberGenerator.randomNumber(1, 10);
        List<Tag> tags = new ArrayList<>(tagSize);
        for(int i=0; i<tagSize; i++) {
            tagRepository.findById((long)numberGenerator.randomNumber(1, 1347)).ifPresent(tags::add);
        }
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName(item);
        giftCertificate.setDescription(String.format("Gift certificate named '%s'", item));
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.UP);
        giftCertificate.setPrice(Double.parseDouble(decimalFormat.format(numberGenerator.randomNumber(tagSize, 100.5))));
        giftCertificate.setDuration(numberGenerator.randomNumber(5, 150));
        giftCertificate.getTags().addAll(tags);
        return giftCertificate;
    }

}
