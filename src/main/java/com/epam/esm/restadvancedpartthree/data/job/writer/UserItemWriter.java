package com.epam.esm.restadvancedpartthree.data.job.writer;

import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class UserItemWriter implements ItemWriter<User> {
    private final UserRepository userRepository;
    public UserItemWriter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void write(List<? extends User> items) throws Exception {
        userRepository.saveAll(items);
    }
}
