package com.epam.esm.restadvancedpartthree.data.job.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCredential {
    private String firstName;
    private String lastName;
    private String email;
}
