package com.epam.esm.restadvancedpartthree.data.job.writer;

import com.epam.esm.restadvancedpartthree.repository.GiftCertificateRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class GiftCertificateWriter implements ItemWriter<GiftCertificate> {
    private final GiftCertificateRepository giftCertificateRepository;
    public GiftCertificateWriter(GiftCertificateRepository giftCertificateRepository) {
        this.giftCertificateRepository = giftCertificateRepository;
    }

    @Override
    public void write(List<? extends GiftCertificate> items) throws Exception {
        giftCertificateRepository.saveAll(items);
    }
}
