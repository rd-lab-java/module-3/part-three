package com.epam.esm.restadvancedpartthree.data.job.writer;

import com.epam.esm.restadvancedpartthree.repository.OrderRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;

import java.util.List;

public class OrderWriter implements ItemWriter<Order> {
    private final RepositoryItemWriter<Order> itemWriter;
    public OrderWriter(OrderRepository orderRepository) {
        itemWriter = new RepositoryItemWriter<>();
        itemWriter.setRepository(orderRepository);
    }

    @Override
    public void write(List<? extends Order> items) throws Exception {
        itemWriter.write(items);
    }
}
