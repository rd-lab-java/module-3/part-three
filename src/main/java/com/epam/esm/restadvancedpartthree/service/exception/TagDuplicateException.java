package com.epam.esm.restadvancedpartthree.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Tag Duplicate Exception")
public class TagDuplicateException extends RuntimeException {
	private static final long serialVersionUID = 6831577596699482194L;

	public TagDuplicateException() {
		super();
	}

	public TagDuplicateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TagDuplicateException(String message, Throwable cause) {
		super(message, cause);
	}

	public TagDuplicateException(String message) {
		super(message);
	}

	public TagDuplicateException(Throwable cause) {
		super(cause);
	}
}
