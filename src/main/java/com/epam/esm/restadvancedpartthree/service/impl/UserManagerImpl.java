package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.repository.UserRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.User;
import com.epam.esm.restadvancedpartthree.service.UserManager;
import com.epam.esm.restadvancedpartthree.service.exception.UserDuplicateException;
import com.epam.esm.restadvancedpartthree.service.exception.UserNotFoundException;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
public class UserManagerImpl implements UserManager {
    private final UserRepository userRepository;
    private final Environment environment;

    public UserManagerImpl(UserRepository userRepository, Environment environment) {
        this.userRepository = userRepository;
        this.environment = environment;
    }

    @Transactional
    @Override
    public User save(User user) {
        try {
            return userRepository.saveAndFlush(user);
        } catch (DataIntegrityViolationException ex) {
            throw new UserDuplicateException(
                    String.format(environment.getProperty("user.message.duplicate"), user.getFullName()));
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public User findById(long id) {
        return userRepository.findById(id).<UserNotFoundException>orElseThrow(() -> {
            throw new UserNotFoundException(String.format(environment.getProperty("user.message.notFound"), id));
        });
    }

    @Transactional
    @Override
    public User deleteById(long userId) {
        User user = userRepository.findById(userId).<UserNotFoundException>orElseThrow(() -> {
            throw new UserNotFoundException(String.format(environment.getProperty("user.message.notFound"), userId));
        });
        userRepository.deleteById(userId);
        return user;
    }

    @Transactional
    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }
}
