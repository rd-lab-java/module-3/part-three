package com.epam.esm.restadvancedpartthree.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Tag Not Found Exception")
public class TagNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9188431423280527994L;

	public TagNotFoundException() {
		super();
	}

	public TagNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TagNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public TagNotFoundException(String message) {
		super(message);
	}

	public TagNotFoundException(Throwable cause) {
		super(cause);
	}

}
