package com.epam.esm.restadvancedpartthree.service.impl;

import com.epam.esm.restadvancedpartthree.repository.OrderRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import com.epam.esm.restadvancedpartthree.service.CostCalculator;
import com.epam.esm.restadvancedpartthree.service.OrderManager;
import com.epam.esm.restadvancedpartthree.service.exception.OrderDuplicateException;
import com.epam.esm.restadvancedpartthree.service.exception.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
public class OrderManagerImpl implements OrderManager {
    private final OrderRepository orderRepository;
    private final CostCalculator costCalculator;
    private final Environment environment;

    @Autowired
    public OrderManagerImpl(OrderRepository orderRepository, CostCalculator costCalculator, Environment environment) {
        this.orderRepository = orderRepository;
        this.costCalculator = costCalculator;
        this.environment = environment;
    }

    @Transactional
    @Override
    public Order save(Order order) {
        order.setTotalCost(costCalculator.totalCost(order.getQuantity(), order.getUnitPrice()));
        try {
            return orderRepository.saveAndFlush(order);
        } catch (DataIntegrityViolationException ex) {
            throw new OrderDuplicateException(String.format(environment.getProperty("order.message.duplicate"),
                    order.getOwner().getId(), order.getGiftCertificate().getId()), ex);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Order> findByGiftCertificateId(long giftCertificateId, Pageable pageable) {
        return orderRepository.findByGiftCertificateId(giftCertificateId, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Order> findByOwnerId(long ownerId, Pageable pageable) {
        return orderRepository.findByOwnerId(ownerId, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Order findById(long orderId) {
        return orderRepository.findById(orderId).<OrderNotFoundException>orElseThrow(() -> {
            throw new OrderNotFoundException(
                    String.format(environment.getProperty("order.message.notFoundById"), orderId));
        });
    }

    @Transactional
    @Override
    public Order deleteById(long id) {
        Order order = orderRepository.findById(id).<OrderNotFoundException>orElseThrow(() -> {
            throw new OrderNotFoundException(String.format(environment.getProperty("order.message.notFoundById"), id));
        });
        orderRepository.deleteById(id);
        return order;
    }

    @Transactional
    @Override
    public void deleteAll() {
        orderRepository.deleteAll();
    }

}
