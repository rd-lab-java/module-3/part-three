package com.epam.esm.restadvancedpartthree.service.exception;

public class IllegalOrderStatusValueException extends Exception {
    public IllegalOrderStatusValueException(String value) {
        super("Order status value is not compatible with predefined status: " + value);
    }
}
