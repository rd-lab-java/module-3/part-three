package com.epam.esm.restadvancedpartthree.service;

public interface CostCalculator {
    double totalCost(int quantity, double price);
}
