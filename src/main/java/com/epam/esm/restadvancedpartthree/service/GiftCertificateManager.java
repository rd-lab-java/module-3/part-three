package com.epam.esm.restadvancedpartthree.service;

import com.epam.esm.restadvancedpartthree.repository.entity.GiftCertificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Optional;


/**
 * This interface imposes business logic 
 * on gift certificate class. 
 * @author yusuf
 */
@Component
public interface GiftCertificateManager {
	GiftCertificate save(GiftCertificate giftCertificate);
	Page<GiftCertificate> findAll(Pageable pageable);
	GiftCertificate findById(long id);
	GiftCertificate update(GiftCertificate giftCertificate);
	GiftCertificate deleteById(long id);
	Page<GiftCertificate> findByTagNames(Optional<String[]> tagNames, Pageable pageable);
	Page<GiftCertificate> findByNameContaining(Optional<String> partOfName, Pageable pageable);
	Page<GiftCertificate> findByDescriptionContaining(Optional<String> partOfDesc, Pageable pageable);
    Page<GiftCertificate> findByTagId(long tagId, Pageable pageable);

    void deleteAll();
}
