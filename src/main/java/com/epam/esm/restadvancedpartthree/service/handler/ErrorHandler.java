package com.epam.esm.restadvancedpartthree.service.handler;

import com.epam.esm.restadvancedpartthree.service.exception.*;
import com.epam.esm.restadvancedpartthree.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
@PropertySource(name= "error_code-values.properties", value = "classpath:error_code-values.properties")
public class ErrorHandler {
	
	@Autowired 
	private Environment environment;
	
	@ExceptionHandler(GiftCertificateNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseUtils giftCertNotFoundExceptionHandler(GiftCertificateNotFoundException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("giftCertificate.code.notFound", Integer.class),
				ex.getMessage(), null);
	}
	
	@ExceptionHandler(GiftCertificateDuplicateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils giftCertDuplicateExceptionHandler(GiftCertificateDuplicateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("giftCertificate.code.internalServerError",
				Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(GiftCertificateNotModifiedException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils giftCertNotModifiedExceptionHandler(GiftCertificateNotModifiedException ex,
															 WebRequest request) {
		System.out.println("Not modified exception thrown");
		return ResponseUtils.response(environment.getProperty("giftCertificate.code.internalServerError",
				Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(TagNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseUtils tagNotFoundExceptionHandler(TagNotFoundException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("tag.code.notFound", Integer.class),
				ex.getMessage(), null);
	}
	
	@ExceptionHandler(TagDuplicateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils tagDuplicateExceptionHandler(TagDuplicateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("tag.code.internalServerError", Integer.class),
				ex.getMessage(), null);
	}
	
	@ExceptionHandler(IllegalStateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils illegalStateExceptionHandler(IllegalStateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("giftCertificate.code.internalServerError",
				Integer.class), ex.getMessage(), null);
	}

	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseUtils userNotFoundExceptionHandler(UserNotFoundException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("user.code.notFound", Integer.class),
				ex.getMessage(), null);
	}

	@ExceptionHandler(UserDuplicateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils userDuplicateExceptionHandler(UserDuplicateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("user.code.internalServerError", Integer.class),
				ex.getMessage(), null);
	}

	@ExceptionHandler(OrderDuplicateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils orderDuplicateExceptionHandler(OrderDuplicateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("order.code.internalServerError", Integer.class),
				ex.getMessage(), null);
	}

	@ExceptionHandler(OrderNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseUtils orderNotFoundExceptionHandler(OrderNotFoundException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("order.code.notFound", Integer.class),
				ex.getMessage(), null);
	}
}
