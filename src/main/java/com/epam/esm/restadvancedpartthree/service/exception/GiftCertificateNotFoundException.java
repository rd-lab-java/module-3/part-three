package com.epam.esm.restadvancedpartthree.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Gift Certificate Not Found")
public class GiftCertificateNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1575953473563838894L;
	
	public GiftCertificateNotFoundException() {
		super();
	}

	public GiftCertificateNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GiftCertificateNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public GiftCertificateNotFoundException(String message) {
		super(message);
	}

	public GiftCertificateNotFoundException(Throwable cause) {
		super(cause);
	}

}
