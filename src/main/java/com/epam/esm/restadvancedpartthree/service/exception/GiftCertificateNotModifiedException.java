package com.epam.esm.restadvancedpartthree.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_MODIFIED, reason = "Gift Certificate content not modified")
public class GiftCertificateNotModifiedException extends RuntimeException {

	private static final long serialVersionUID = 2953397588642198502L;

	public GiftCertificateNotModifiedException() {
		super();
	}

	public GiftCertificateNotModifiedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GiftCertificateNotModifiedException(String message, Throwable cause) {
		super(message, cause);
	}

	public GiftCertificateNotModifiedException(String message) {
		super(message);
	}

	public GiftCertificateNotModifiedException(Throwable cause) {
		super(cause);
	}
	
}
