package com.epam.esm.restadvancedpartthree.service;

import com.epam.esm.restadvancedpartthree.repository.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderManager {
    Order save(Order order);
    Page<Order> findByGiftCertificateId(long giftCertificateId, Pageable pageable);
    Page<Order> findByOwnerId(long ownerId, Pageable pageable);
    Order findById(long orderId);
    Order deleteById(long id);

    void deleteAll();
}
