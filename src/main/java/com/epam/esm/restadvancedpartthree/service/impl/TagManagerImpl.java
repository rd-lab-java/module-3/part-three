package com.epam.esm.restadvancedpartthree.service.impl;


import com.epam.esm.restadvancedpartthree.repository.TagRepository;
import com.epam.esm.restadvancedpartthree.repository.entity.Tag;
import com.epam.esm.restadvancedpartthree.service.TagManager;
import com.epam.esm.restadvancedpartthree.service.exception.TagDuplicateException;
import com.epam.esm.restadvancedpartthree.service.exception.TagNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Tag Service interface implementation that contains 
 * implementation of CRUD opertations business logics
 * 
 * @author yusuf
 *
 */
@Service
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
public class TagManagerImpl implements TagManager {
	private final TagRepository tagRepository;
	private final Environment environment;

	@Autowired
	public TagManagerImpl(TagRepository tagRepository, Environment environment) {
		this.tagRepository = tagRepository;
		this.environment = environment;
	}

	@Override
	@Transactional
	public Tag save(Tag tag) {
		try {
			return tagRepository.saveAndFlush(tag);
		} catch (DataIntegrityViolationException exception) {
			throw new TagDuplicateException(String.format(
					environment.getProperty("tag.message.duplicate"), tag.getName()));
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tag> findByGiftCertificateId(long giftCertificateId, Pageable pageable) {
		return tagRepository.findByGiftCertificateId(giftCertificateId, pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tag> findAll(Pageable pageable) {
		return tagRepository.findAll(pageable);
	}

	@Override
	@Transactional
	public Tag deleteById(long id) {
        Tag tag = tagRepository.findById(id).<TagNotFoundException>orElseThrow(() -> {
            throw new TagNotFoundException(String.format(environment.getProperty("tag.message.notFoundById"), id));
        });
        tagRepository.deleteById(id);
        return tag;
    }

	@Override
	@Transactional(readOnly = true)
	public Tag findById(long id) {
		return tagRepository.findById(id).<TagNotFoundException>orElseThrow(() -> {
		    throw new TagNotFoundException(String.format(environment.getProperty("tag.message.notFoundById"), id));
        });
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tag> findMostUsedTags(Pageable pageable) {
		return tagRepository.findMostUsedTags(pageable);
	}

    @Transactional
	@Override
	public void deleteAll() {
		tagRepository.deleteAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tag> findTopMostUsedTags(int top) {
		return tagRepository.findTopNumberMostUsedTags(top);
	}
}
