package com.epam.esm.restadvancedpartthree.service;

import com.epam.esm.restadvancedpartthree.repository.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserManager {
    User save(User user);
    Page<User> findAll(Pageable pageable);
    User findById(long id);
    User deleteById(long userId);

    void deleteAll();
}
