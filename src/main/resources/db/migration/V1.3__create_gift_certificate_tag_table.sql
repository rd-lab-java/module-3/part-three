CREATE TABLE IF NOT EXISTS gift_certs_tags (
  gift_certificate_id bigint NOT NULL,
  tag_id bigint NOT NULL,
  UNIQUE KEY UKhmnrfxrc808ns233tfksklgie (gift_certificate_id, tag_id),
  KEY tag_id (tag_id),
  KEY gift_certificate_id_idx (gift_certificate_id),

  CONSTRAINT gift_certs_tags_ibfk_1
    FOREIGN KEY (tag_id)
        REFERENCES tag (id)
            ON DELETE CASCADE,

  CONSTRAINT gift_certs_tags_ibfk_2
    FOREIGN KEY (gift_certificate_id)
        REFERENCES gift_certificate (id)
            ON DELETE CASCADE
);
