CREATE TABLE IF NOT EXISTS gift_certificate_order (
  id bigint NOT NULL AUTO_INCREMENT,
  ordered_date datetime DEFAULT NULL,
  last_updated_date datetime DEFAULT NULL,
  quantity int NOT NULL,
  status varchar(255) NOT NULL,
  total_cost double DEFAULT NULL,
  unit_price double DEFAULT NULL,
  gift_certificate_id bigint NOT NULL,
  owner_id bigint NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK83m07r45esyj6lgvd6yugwml8 (owner_id, gift_certificate_id),
  KEY gift_certificate_id (gift_certificate_id),
  KEY total_cost_idx (total_cost),
  CONSTRAINT gift_certificate_order_ibfk_1
    FOREIGN KEY (gift_certificate_id)
        REFERENCES gift_certificate (id)
            ON DELETE CASCADE,

  CONSTRAINT gift_certificate_order_ibfk_2
    FOREIGN KEY (owner_id)
        REFERENCES user(id)
            ON DELETE CASCADE
);
