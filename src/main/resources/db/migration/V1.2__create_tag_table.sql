CREATE TABLE IF NOT EXISTS tag (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_1wdpsed5kna2y38hnbgrnhi5b (name)
);
