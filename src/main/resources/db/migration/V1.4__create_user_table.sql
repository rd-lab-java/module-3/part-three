CREATE TABLE IF NOT EXISTS user (
  id bigint NOT NULL AUTO_INCREMENT,
  full_name varchar(255) DEFAULT NULL,
  email varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  created_date datetime NOT NULL,
  last_updated_date datetime NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (email)
);
