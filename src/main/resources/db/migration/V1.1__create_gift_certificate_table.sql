CREATE TABLE IF NOT EXISTS gift_certificate (
  id bigint NOT NULL AUTO_INCREMENT,
  created_date datetime NOT NULL,
  description varchar(255) DEFAULT NULL,
  duration int NOT NULL,
  name varchar(255) NOT NULL,
  price double NOT NULL,
  last_updated_date datetime NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_en7vlwcn343hrd7cfcibpw284 (name)
);
