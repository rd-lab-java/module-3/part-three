#### Part 3

This sub-module covers following topics:
1. ORM
2. JPA & Hibernate
3. Transactions
ORM stands for Object Relational Mapping. It’s a bit of an abstract concept – but basically it’s a technique that allows us to query and change data from the database in an object oriented way. ORMs provide a high-level abstraction upon a relational database that allows a developer to write Java code instead of SQL to create, read, update and delete data and schemas in their database. Developers can use the programming language they are comfortable with to work with a database instead of writing SQL statements or stored procedures. A JPA (Java Persistence API) is a specification of Java which is used to access, manage, and persist data between Java object and relational database. It is considered as a standard approach for Object Relational Mapping. JPA can be seen as a bridge between object-oriented domain models and relational database systems. Being a specification, JPA doesn't perform any operation by itself. Thus, it requires implementation. So, ORM tools like Hibernate, TopLink, and iBatis implements JPA specifications for data persistence. A transaction usually means a sequence of information exchange and related work (such as database updating) that is treated as a unit for the purposes of satisfying a request and for ensuring database integrity. For a transaction to be completed and database changes to made permanent, a transaction has to be completed in its entirety.

##### Application requirements

1. Hibernate should be used as a JPA implementation for data access.
2. Spring Transaction should be used in all necessary areas of the application.
3. Audit data should be populated using JPA features (an example can be found in materials).

##### Application restrictions

1. Hibernate specific features.
2. Spring Data

# Demo
## Practical part

1. Generate for a demo at least 
     - 1000 users
     - 1000 tags
     - 10’000 gift certificates (should be linked with tags and users)
All values should look like more -or-less meaningful: random words, but not random letters 
2. Demonstrate API using Postman tool (prepare for demo Postman collection with APIs)  
3. (Optional) Build & run application using command line

#### Getting Started
1. Running applicating with spring batch job, which is to populate large sample data into database.

```shell script
java -jar "application jar file(.jar)" "job parameters(e.g "run.date(date)=2021/08/24")"
```
2. Run application without batch job.

     first disable batch processing, add propertied in application.properties file.

     ``` gradle
     spring.batch.job.enabled=false
     ```
     then just run spring application.

     ```shell script
     java -jar "application jar file(.jar)"
     ```

